/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// You can delete this file if you're not using it

const path = require(`path`)

exports.onCreateWebpackConfig = ({ stage, loaders, actions }) => {
  if (stage === "build-html") {
    actions.setWebpackConfig({
      module: {
        rules: [
          {
            test: /masonry-layout/,
            use: loaders.null(),
          },
        ],
      },
    })
  }
}

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  let allCategory = ["all"]

  const productPages = await graphql(`
    {
      allShopifyProduct {
        edges {
          node {
            handle
          }
        }
      }
    }
  `)

  productPages.data.allShopifyProduct.edges.forEach(({ node }) => {
    createPage({
      path: `/products/${node.handle}/`,
      component: path.resolve(`./src/templates/productpage.js`),
      context: {
        // Data passed to context is available
        // in page queries as GraphQL variables.
        handle: node.handle,
      },
    })
  })

  const frequentlyAskedQuestionsPage = await graphql(`
    {
      prismicFrequentlyAskedQuestions {
        id
        data {
          text {
            html
          }
          title {
            html
            text
          }
          h1_text_align
          regular_text_align
        }
        slugs
      }
    }
  `)

  createPage({
    path: `/${
      frequentlyAskedQuestionsPage.data.prismicFrequentlyAskedQuestions.slugs[0]
    }/`,
    component: path.resolve(`./src/templates/infopage.js`),
    context: {
      // Data passed to context is available
      // in page queries as GraphQL variables.
      data:
        frequentlyAskedQuestionsPage.data.prismicFrequentlyAskedQuestions.data,
    },
  })

  const imprintPage = await graphql(`
    {
      prismicImprint {
        id
        data {
          text {
            html
          }
          title {
            html
            text
          }
          h1_text_align
          regular_text_align
        }
        slugs
      }
    }
  `)

  createPage({
    path: `/${imprintPage.data.prismicImprint.slugs[0]}/`,
    component: path.resolve(`./src/templates/infopage.js`),
    context: {
      // Data passed to context is available
      // in page queries as GraphQL variables.
      data: imprintPage.data.prismicImprint.data,
    },
  })

  const privacyPolicyPage = await graphql(`
    {
      prismicPrivacyPolicy {
        id
        data {
          text {
            html
          }
          title {
            html
            text
          }
          h1_text_align
          regular_text_align
        }
        slugs
      }
    }
  `)

  createPage({
    path: `/${privacyPolicyPage.data.prismicPrivacyPolicy.slugs[0]}/`,
    component: path.resolve(`./src/templates/infopage.js`),
    context: {
      // Data passed to context is available
      // in page queries as GraphQL variables.
      data: privacyPolicyPage.data.prismicPrivacyPolicy.data,
    },
  })

  const shippingAndReturnsPage = await graphql(`
    {
      prismicShippingAndReturns {
        id
        data {
          text {
            html
          }
          title {
            html
            text
          }
          h1_text_align
          regular_text_align
        }
        slugs
      }
    }
  `)

  createPage({
    path: `/${shippingAndReturnsPage.data.prismicShippingAndReturns.slugs[0]}/`,
    component: path.resolve(`./src/templates/infopage.js`),
    context: {
      // Data passed to context is available
      // in page queries as GraphQL variables.
      data: shippingAndReturnsPage.data.prismicShippingAndReturns.data,
    },
  })

  const termsOfUsePage = await graphql(`
    {
      prismicTermsOfUse {
        id
        data {
          text {
            html
          }
          title {
            html
            text
          }
          h1_text_align
          regular_text_align
        }
        slugs
      }
    }
  `)

  createPage({
    path: `/${termsOfUsePage.data.prismicTermsOfUse.slugs[0]}/`,
    component: path.resolve(`./src/templates/infopage.js`),
    context: {
      // Data passed to context is available
      // in page queries as GraphQL variables.
      data: termsOfUsePage.data.prismicTermsOfUse.data,
    },
  })

  const contactPage = await graphql(`
    {
      prismicContact {
        id
        data {
          text {
            html
          }
          title {
            html
            text
          }
          h1_text_align
          regular_text_align
        }
        slugs
      }
    }
  `)

  createPage({
    path: `/${contactPage.data.prismicContact.slugs[0]}/`,
    component: path.resolve(`./src/templates/infopage.js`),
    context: {
      // Data passed to context is available
      // in page queries as GraphQL variables.
      data: contactPage.data.prismicContact.data,
    },
  })

  const berlinStorePage = await graphql(`
    {
      prismicBerlinStore {
        id
        data {
          text {
            html
          }
          title {
            html
            text
          }
          h1_text_align
          regular_text_align
        }
        slugs
      }
    }
  `)

  createPage({
    path: `/${berlinStorePage.data.prismicBerlinStore.slugs[0]}/`,
    component: path.resolve(`./src/templates/infopage.js`),
    context: {
      // Data passed to context is available
      // in page queries as GraphQL variables.
      data: berlinStorePage.data.prismicBerlinStore.data,
    },
  })

  const aboutPage = await graphql(`
    {
      prismicAbout {
        id
        data {
          text {
            html
          }
          title {
            html
            text
          }
          h1_text_align
          regular_text_align
        }
        slugs
      }
    }
  `)

  createPage({
    path: `/${aboutPage.data.prismicAbout.slugs[0]}/`,
    component: path.resolve(`./src/templates/infopage.js`),
    context: {
      // Data passed to context is available
      // in page queries as GraphQL variables.
      data: aboutPage.data.prismicAbout.data,
    },
  })

  const categoryPages = await graphql(`
    {
      allShopifyCollection(sort: { fields: description, order: ASC }) {
        edges {
          node {
            id
            title
          }
        }
      }
    }
  `)

  categoryPages.data.allShopifyCollection.edges.forEach(({ node }) => {
    createPage({
      path: `/${node.title}/`,
      component: path.resolve(`./src/templates/productcategorypage.js`),
      context: {
        // Data passed to context is available
        // in page queries as GraphQL variables.
        id: node.id,
        title: node.title,
      },
    })
  })

  const storyPages = await graphql(`
    {
      allPrismicStory(filter: { uid: { ne: "placeholderslices" } }) {
        edges {
          node {
            uid
            id
            data {
              title {
                text
              }
            }
          }
        }
      }
    }
  `)

  storyPages.data.allPrismicStory.edges.forEach(({ node }) => {
    createPage({
      path: `/${node.uid}/`,
      component: path.resolve(`./src/templates/storypage.js`),
      context: {
        // Data passed to context is available
        // in page queries as GraphQL variables.
        id: node.id,
        title: node.data.title.text,
      },
    })
  })

  // allCategory.forEach(element => {
  //   createPage({
  //     path: `/${element}/`,
  //     component: path.resolve(`./src/templates/productcategoryallpage.js`),
  //     context: {
  //       // Data passed to context is available
  //       // in page queries as GraphQL variables.
  //       uri: element,
  //     },
  //   })
  // })
}
