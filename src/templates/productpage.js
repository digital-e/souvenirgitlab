import React from "react"
import { graphql } from "gatsby"
import styled from "styled-components"
import Img from "gatsby-image"

import posed from "react-pose"

import SEO from "../components/seo"

import Footer from "../components/footer"

import SouvenirTicker from "../components/souvenir-ticker"

import ProductsGrid from "../components/productsgrid"

import ProductBuyContainer from "../components/productbuycontainer"

const Product = styled.div`
  display: flex;
  flex-direction: column;

  @media (min-width: 576px) {
    display: flex;
    flex-direction: row;
  }
`

const ProductPanelLeft = styled.div`
  flex-basis: 50%;
`

const ProductPanelRight = styled.div`
  flex-basis: 50%;
  display: flex;
  justify-content: center;
  padding-top: 2rem;
  padding-bottom: 2rem;

  @media (min-width: 576px) {
    flex-basis: 50%;
    display: flex;
    justify-content: center;
    padding-top: 0;
    padding-bottom: 0;
  }
`

class ProductPage extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      isOpenProductDetails: false,
      isOpenShipping: false,
      productPanelRightHeight: 0,
      productBuyContainerheight: 0,
    }

    this.toggleDropdownOne = this.toggleDropdownOne.bind(this)
    this.toggleDropdownTwo = this.toggleDropdownTwo.bind(this)

    this.productBuyContainerRef = React.createRef()
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        productPanelRightHeight: this.productPanelRightRef
          ? this.productPanelRightRef.getBoundingClientRect().height
          : 0,
        // productBuyContainerHeight: this.productBuyContainerRef.getBoundingClientRect()
        //   .height,
      })
    }, 0)

    setTimeout(() => {
      window.scrollTo(0, 0)
    }, 450)
  }

  toggleDropdownOne() {
    this.setState(prevState => ({
      isOpenProductDetails: !prevState.isOpenProductDetails,
    }))
  }

  toggleDropdownTwo() {
    this.setState(prevState => ({
      isOpenShipping: !prevState.isOpenShipping,
    }))
  }

  render() {
    return (
      <>
        <SEO title={this.props.data.shopifyProduct.title} />
        <Product>
          <ProductPanelLeft>
            {this.props.data.shopifyProduct.images.length < 3
              ? this.props.data.shopifyProduct.images.map(item => (
                  <Img
                    fluid={item ? item.localFile.childImageSharp.fluid : null}
                  />
                ))
              : this.props.data.shopifyProduct.images.map((item, index) => {
                  if (index < 2) return
                  return (
                    <Img
                      fluid={item ? item.localFile.childImageSharp.fluid : null}
                    />
                  )
                })}
          </ProductPanelLeft>
          <ProductPanelRight ref={el => (this.productPanelRightRef = el)}>
            <ProductBuyContainer
              product={this.props.data.shopifyProduct}
              productPanelRightHeight={this.state.productPanelRightHeight}
              shippingInfo={
                this.props.data.prismicProductShippingInformation.data.text.html
              }
            />
          </ProductPanelRight>
        </Product>
        <SouvenirTicker fixed={false} />
        <ProductsGrid allProducts={this.props.data.allShopifyProduct} />
        <Footer />
      </>
    )
  }
}

export default ProductPage

export const query = graphql`
  query($handle: String!) {
    shopifyProduct(handle: { eq: $handle }) {
      id
      title
      handle
      productType
      descriptionHtml
      shopifyId
      options {
        id
        name
        values
      }
      variants {
        id
        title
        price
        availableForSale
        shopifyId
        selectedOptions {
          name
          value
        }
      }
      images {
        originalSrc
        id
        localFile {
          childImageSharp {
            fluid(maxWidth: 910, quality: 90) {
              ...GatsbyImageSharpFluid_withWebp_noBase64
            }
          }
        }
      }
    }

    allShopifyProduct(sort: { fields: [createdAt], order: DESC }) {
      edges {
        node {
          availableForSale
          id
          title
          handle
          createdAt
          images {
            id
            originalSrc
            localFile {
              childImageSharp {
                fluid(maxWidth: 910, quality: 90) {
                  ...GatsbyImageSharpFluid_noBase64
                }
              }
            }
          }
          variants {
            price
            availableForSale
            title
          }
          availableForSale
        }
      }
    }

    prismicProductShippingInformation {
      data {
        text {
          html
        }
      }
    }
  }
`
