import React from "react"

import styled from "styled-components"

import SEO from "../components/seo"

import Footer from "../components/footer"

const Container = styled.div`
  font-family: "Arial Narrow";
  font-size: 1rem;
  margin: 10rem auto;
  text-align: ${props => props.regularcenter};
  min-height: 100vh;
  width: 80%;
  box-sizing: border-box;

  @media (min-width: 576px) {
    width: 80%;
  }

  a {
    color: rgb(255, 55, 252);
    text-decoration: none;
  }

  strong {
    font-weight: bold !important;
  }

  p {
    line-height: 1.5rem;
  }

  a:hover {
    text-decoration: underline;
  }

  h1 {
    font-size: 1.5rem;
    text-align: ${props => props.hcenter};
  }
`

class InfoPage extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
    setTimeout(() => {
      window.scrollTo(0, 0)
    }, 450)
  }

  render() {
    return (
      <>
        <SEO title={this.props.pageContext.data.title.text} />
        <Container
          hcenter={this.props.pageContext.data.h1_text_align}
          regularcenter={this.props.pageContext.data.regular_text_align}
        >
          <div
            dangerouslySetInnerHTML={{
              __html: this.props.pageContext.data.text.html,
            }}
          />
        </Container>
        <Footer />
      </>
    )
  }
}

export default InfoPage
