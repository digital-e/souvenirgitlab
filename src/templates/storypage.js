import React from "react"

import styled from "styled-components"

import SEO from "../components/seo"

import Footer from "../components/footer"

import StoriesGrid from "../components/storiesgrid"

import {
  TitleBlock,
  TitleBlockWithImage,
  BigQuote,
  Image,
  Text,
  ImageGalleryX2,
  ImageGalleryX3,
} from "../components/slices/index"

const Container = styled.div`
  font-family: "Arial Narrow";
  font-size: 1rem;
  background-color: ${props => props.backgroundcolour};
  color: ${props => props.textcolour};
`

const Spacer = styled.div`
  height: 150px;
  background-color: ${props => props.backgroundcolour};
`

class StoryPage extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
    setTimeout(() => {
      window.scrollTo(0, 0)
    }, 450)
  }

  renderSlices = slice => {
    switch (slice.slice_type) {
      case "title_block":
        return <TitleBlock data={slice.primary} />
        break
      case "title_block_with_image":
        return <TitleBlockWithImage data={slice.primary} />
        break
      case "big_quote":
        return <BigQuote data={slice.primary} />
        break
      case "image":
        return <Image data={slice.primary} />
        break
      case "text":
        return <Text data={slice.primary} />
        break
      case "image-gallery_x_2":
        return <ImageGalleryX2 data={slice.primary} />
      case "image-gallery_x_3":
        return <ImageGalleryX3 data={slice.primary} />
        break
      default:
        return null
    }
  }

  render() {
    let { data } = this.props.data.prismicStory

    return (
      <>
        <SEO title={this.props.pageContext.title} />
        <Spacer backgroundcolour={data.background_colour} />
        <Container
          backgroundcolour={data.background_colour}
          textcolour={data.text_colour}
        >
          {data.body.map(slice => this.renderSlices(slice))}
          <StoriesGrid />
          <Footer />
        </Container>
      </>
    )
  }
}

export default StoryPage

export const storyQuery = graphql`
  query StoryById($id: String!) {
    prismicStory(id: { eq: $id }) {
      data {
        body {
          ... on PrismicStoryBodyBigQuote {
            id
            primary {
              alignment
              quote {
                html
              }
            }
            slice_type
          }
          ... on PrismicStoryBodyImage {
            id
            primary {
              credits_alignment
              image_alignment
              image {
                url
                localFile {
                  childImageSharp {
                    fluid(maxWidth: 2500) {
                      ...GatsbyImageSharpFluid_noBase64
                    }
                  }
                }
              }
              credits {
                html
              }
            }
            slice_type
          }
          ... on PrismicStoryBodyImageGalleryX2 {
            id
            slice_type
            primary {
              image_one {
                alt
                copyright
                url
                localFile {
                  childImageSharp {
                    fluid(maxWidth: 1500) {
                      ...GatsbyImageSharpFluid_noBase64
                    }
                  }
                }
              }
              image_two {
                alt
                url
                localFile {
                  childImageSharp {
                    fluid(maxWidth: 1500) {
                      ...GatsbyImageSharpFluid_noBase64
                    }
                  }
                }
              }
              credits_image_one {
                html
              }
              credits_image_two {
                html
              }
            }
          }
          ... on PrismicStoryBodyImageGalleryX3 {
            id
            primary {
              image_one {
                url
                localFile {
                  childImageSharp {
                    fluid(maxWidth: 910) {
                      ...GatsbyImageSharpFluid_noBase64
                    }
                  }
                }
              }
              image_two {
                url
                localFile {
                  childImageSharp {
                    fluid(maxWidth: 910) {
                      ...GatsbyImageSharpFluid_noBase64
                    }
                  }
                }
              }
              image_three {
                url
                localFile {
                  childImageSharp {
                    fluid(maxWidth: 910) {
                      ...GatsbyImageSharpFluid_noBase64
                    }
                  }
                }
              }
              credits_image_one {
                html
              }
              credits_image_two {
                html
              }
              credits_image_three {
                html
              }
            }
            slice_type
          }
          ... on PrismicStoryBodyText {
            id
            primary {
              text_field {
                html
              }
              alignment
            }
            slice_type
          }
          ... on PrismicStoryBodyTitleBlock {
            id
            primary {
              credits {
                html
              }
              text {
                html
              }
              title {
                text
              }
            }
            slice_type
          }
          ... on PrismicStoryBodyTitleBlockWithImage {
            id
            slice_type
            primary {
              image_align
              credits {
                html
              }
              image {
                alt
                localFile {
                  childImageSharp {
                    fluid(maxWidth: 910) {
                      ...GatsbyImageSharpFluid_noBase64
                    }
                  }
                }
              }
              text {
                html
              }
              title {
                text
              }
            }
          }
        }
        text_colour
        title {
          text
        }
        background_colour
      }
    }
  }
`
