import React, { useState } from "react"

import { Link } from "gatsby"

import styled from "styled-components"

const Container = styled.div`
  position: relative;
  float: left;
  width: 100%;
  height: 150vw;

  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }

  @media (min-width: 576px) {
    width: 25%;
    height: 32vw;
  }
`

const ImageContainer = styled.div`
  position: absolute;
  height: 100%;
  width: 100%;
`

const Information = styled.div`
  position: absolute;
  font-family: "Arial Narrow";
  font-size: 2rem;
  padding: 1rem;
  color: white;
  height: 100%;
  width: 100%;
  display: none;

  @media (min-width: 576px) {
    display: block;
  }
`

const InformationMobile = styled.div`
  position: absolute;
  font-family: "Arial Narrow";
  font-size: 2rem;
  padding: 1rem;
  color: white;
  height: 100%;
  width: 100%;
  display: block;

  @media (min-width: 576px) {
    display: none;
  }
`

const GridItemQuarter = props => {
  const [isHovered, setIsHovered] = useState(false)

  return (
    <Link to={props.data.uid}>
      <Container
        onMouseEnter={() => setIsHovered(true)}
        onMouseLeave={() => setIsHovered(false)}
      >
        <ImageContainer className="image">
          <img src={props.data.data.thumbnail.url} />
        </ImageContainer>
        <Information
          style={
            isHovered
              ? { backgroundColor: "black" }
              : { backgroundColor: "transparent" }
          }
          dangerouslySetInnerHTML={{
            __html: props.data.data.thumbnail_subtitle.html,
          }}
        />
        <InformationMobile
          style={
            isHovered
              ? { backgroundColor: "black" }
              : { backgroundColor: "transparent" }
          }
          dangerouslySetInnerHTML={{
            __html: props.data.data.thumbnail_subtitle__mobile_.html,
          }}
        />
      </Container>
    </Link>
  )
}

export default GridItemQuarter
