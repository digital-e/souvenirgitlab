import React from "react"

import styled from "styled-components"

import posed from "react-pose"

import { Controller, Scene } from "react-scrollmagic"

import ProductForm from "./productForm"

const ProductBuyContainer = styled.div`
  width: 50%;
  height: 100%;
  font-size: 1rem;
  line-height: 1.5rem;

  .shipping-dropdown {
    padding-bottom: 2rem;
  }

  .shipping-dropdown-text {
    padding-top: 1rem !important;
  }
`

const Text = styled.div`
  font-family: "Arial Narrow";
  margin-top: 3rem;
`

const preDropdownText = posed.div({
  open: {
    y: 0,
    opacity: 1,
    transition: {
      ease: "easeInOut",
    },
  },
  closed: {
    opacity: 0,
    transition: {
      ease: "easeInOut",
    },
  },
})

const DropdownText = styled(preDropdownText)`
  font-family: "Arial Narrow";
  padding-top: 3rem;
`

const preExpand = posed.div({
  open: {
    delayChildren: 50,
    staggerChildren: 10,
    maxHeight: 300,
    applyAtStart: { display: "block" },
    transition: {
      ease: "easeInOut",
    },
  },
  closed: {
    maxHeight: 0,
    applyAtEnd: { display: "none" },
    transition: {
      ease: "easeInOut",
    },
  },
})

const ProductDetails = styled(preExpand)`
  font-family: "Arial Narrow";
  cursor: pointer;
`
const Shipping = styled(preExpand)`
  font-family: "Arial Narrow";
  cursor: pointer;
  padding-bottom: 2rem;
`
const DropdownTitle = styled.div`
  font-family: "Arial Narrow";
  margin-top: 3rem;
  cursor: pointer;
  display: flex;
  justify-content: space-between;
`
class ProductBuyContainerComponent extends React.PureComponent {
  //   static contextType = StoreContext
  constructor(props) {
    super(props)
    this.state = {
      isOpenProductDetails: false,
      isOpenShipping: false,
      productPanelRightHeight: 0,
      productBuyContainerheight: 0,
      isMobile: false,
    }

    this.duration = 0

    this.toggleDropdownOne = this.toggleDropdownOne.bind(this)
    this.toggleDropdownTwo = this.toggleDropdownTwo.bind(this)
  }

  componentDidMount() {
    if (window.innerWidth < 576) {
      this.setState({
        isMobile: true,
      })
    }

    setTimeout(() => {
      this.setState({
        // productPanelRightHeight: this.productPanelRightRef.getBoundingClientRect()
        //   .height,
        productBuyContainerHeight: this.productBuyContainerRef
          ? this.productBuyContainerRef.getBoundingClientRect().height
          : 0,
      })
    }, 0)
  }

  toggleDropdownOne() {
    this.setState(prevState => ({
      isOpenProductDetails: !prevState.isOpenProductDetails,
    }))
  }

  toggleDropdownTwo() {
    this.setState(prevState => ({
      isOpenShipping: !prevState.isOpenShipping,
    }))
  }

  render() {
    this.duration =
      this.props.productPanelRightHeight -
      this.state.productBuyContainerHeight -
      100
    return (
      <>
        <Controller>
          <Scene
            enabled={this.state.isMobile ? false : true}
            duration={this.duration && this.duration > 0 ? this.duration : 0}
            offset={-150}
            triggerHook="onLeave"
            pin
          >
            <ProductBuyContainer>
              <div ref={el => (this.productBuyContainerRef = el)}>
                <ProductForm product={this.props.product} />
                <Text
                  dangerouslySetInnerHTML={{
                    __html: this.props.product.descriptionHtml,
                  }}
                />
                {/* <DropdownTitle onClick={this.toggleDropdownOne}>
                  Product Details{" "}
                  <span>{this.state.isOpenProductDetails ? "-" : "+"}</span>
                </DropdownTitle>
                <ProductDetails
                  pose={this.state.isOpenProductDetails ? "open" : "closed"}
                >
                  <DropdownText
                    dangerouslySetInnerHTML={{
                      __html:
                        "- 100% Cotton<br>- Regular fit<br>- Unisex <br>- Souvenir logo & signet embroidery<br>- Delivered folded in biodegradable polybag<br>- Made in Portugal by certified eco friendly factories",
                    }}
                  />
                </ProductDetails> */}
                <DropdownTitle
                  onClick={this.toggleDropdownTwo}
                  className="shipping-dropdown"
                >
                  Shipping
                  <span>{this.state.isOpenShipping ? "-" : "+"}</span>
                </DropdownTitle>
                <Shipping pose={this.state.isOpenShipping ? "open" : "closed"}>
                  <DropdownText
                    className="shipping-dropdown-text"
                    dangerouslySetInnerHTML={{
                      __html: this.props.shippingInfo,
                    }}
                  />
                </Shipping>
              </div>
            </ProductBuyContainer>
          </Scene>
        </Controller>
      </>
    )
  }
}

export default ProductBuyContainerComponent
