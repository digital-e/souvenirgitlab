import React from "react"
import styled from "styled-components"

const Quote = styled.div`
  font-family: "Arial Narrow Bold";
  text-align: center;
  font-size: 2rem;
  width: 100%;
  line-height: 2rem;
  margin: 0 auto;
  word-break: break-word;

  @media (min-width: 992px) {
    text-align: center;
    font-size: 5rem;
    width: ${props =>
      props.centred === "Centred With Margins" ? "40%" : "100%"};
    line-height: 5rem;
    margin: 0 auto;
  }
`

const Container = styled.div`
  margin: 6rem 1rem;

  @media (min-width: 992px) {
    margin: 6rem 2rem;
  }
`

export default ({ ...props }) => {
  let { data } = props
  return (
    <Container>
      <Quote
        dangerouslySetInnerHTML={{ __html: data.quote.html }}
        centred={props.alignment}
      />
    </Container>
  )
}
