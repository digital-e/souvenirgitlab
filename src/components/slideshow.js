import React from "react"
import styled from "styled-components"
import { Link } from "gatsby"

import StoriesGrid from "./storiesgrid"

import Img from "gatsby-image"

const Container = styled.div`
  height: 100vh;
  width: 100vw;
  object-fit: contain;
  position: relative;
  z-index: 998;

  .top {
    z-index: 998;
    visibility: visible;
  }
`
// const Text = styled.div`
//   position: absolute;
//   z-index: 999;
//   font-family: "Arial Narrow Bold";
//   font-size: 10vw;
//   color: black;
//   left: 50%;
//   top: 50%;
//   white-space: nowrap;
//   transform: translate(-50%, -50%);
// `

const Text = styled.div`
  position: absolute;
  z-index: 999;
  font-family: "Arial Narrow Bold";
  font-size: 10vw;
  color: rgb(219, 217, 18);
  -webkit-text-stroke: thin black;
  left: 50%;
  top: 50%;
  white-space: nowrap;
  transform: translate(-50%, -50%);
  width: 100vw;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  a {
    text-decoration: none;
    color: inherit;
    -webkit-text-stroke: inherit;
  }
`

const StyledImg = styled(Img)`
  position: absolute !important;
  z-index: 997;
  visibility: hidden;
  height: 100vh;
  width: 100vw;
`

class SlideShow extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {}

    this.interval = null
    this.intervalTime = this.props.data.data.interval_time
      ? this.props.data.data.interval_time
      : 4000
  }

  componentDidMount() {
    let index = 0
    let length = this.props.data.data.gallery.length

    let random = Math.floor(Math.random() * length)

    let images = document.querySelectorAll(".intro-img")

    // this.containerRef.childNodes[index].classList.add("top")
    // this.interval = setInterval(() => {
    //   this.containerRef.childNodes.forEach(item => {
    //     item.classList.remove("top")
    //   })
    //   this.containerRef.childNodes[index].classList.add("top")
    //   index++
    //   if (index > length - 1) index = 0
    // }, this.intervalTime)

    images[index].classList.add("top")

    images.forEach(item => {
      item.classList.remove("top")
    })

    images[random].classList.add("top")

    setTimeout(() => {
      if (this.titleRef) {
        this.titleRef.classList.add("show")
        this.titleRef.classList.remove("hide")
      }
    }, 500)
  }

  componentWillUnmount() {
    // clearInterval(this.interval)
  }

  render() {
    return (
      <>
        <Link to="/ALL/">
          <Container ref={el => (this.containerRef = el)}>
            {/* <Text ref={el => (this.titleRef = el)} className="hide">
              SOUVENIR OFFICIAL
            </Text> */}
            <Text ref={el => (this.titleRef = el)} className="hide">
              <Link to="/stories/">STORIES</Link>
              <Link to="/ALL/">SHOP</Link>
            </Text>
            {this.props.data.data.gallery.map((item, index) => (
              <StyledImg
                className="intro-img"
                key={index}
                fluid={item.image.localFile.childImageSharp.fluid}
              />
            ))}
          </Container>
        </Link>
        <StoriesGrid />
      </>
    )
  }
}

export default SlideShow
