import React from "react"

import styled from "styled-components"
import ProductThumbnail from "../components/product-thumbnail"

const ProductsGridContainer = styled.div`
  position: relative;
  display: flex;
  width: 100vw;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
  box-sizing: border-box;

  div {
    flex-basis: 100%;
  }

  @media (min-width: 576px) {
    div {
      flex-basis: 50%;
    }
  }

  @media (min-width: 768px) {
    div {
      flex-basis: 33.3%;
    }
  }
`

class ProductsGrid extends React.PureComponent {
  render() {
    return (
      <ProductsGridContainer>
        {this.props.allProducts.edges
          ? this.props.allProducts.edges.map((item, index) =>
              item.node.images.length > 1 ? (
                <ProductThumbnail key={index} data={item} />
              ) : null
            )
          : this.props.allProducts.map((item, index) =>
              item.images.length > 1 ? (
                <ProductThumbnail key={index} data={item} />
              ) : null
            )}
      </ProductsGridContainer>
    )
  }
}

export default ProductsGrid
