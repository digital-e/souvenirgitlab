import React, { useState, useEffect } from "react"
import { useStaticQuery, graphql } from "gatsby"

import styled from "styled-components"

import Ticker from "react-ticker"

const FixedTicker = styled.div`
  position: fixed;
  top: 50%;
  transform: translateY(-50%);
  z-index: 999;
  width: 100%;
  height: auto;
  cursor: pointer;
`

const StandardTicker = styled.div``

const SouvenirTicker = ({ ...props }) => {
  const data = useStaticQuery(graphql`
    query {
      prismicAnnouncementBanner {
        data {
          announcement {
            text
          }
        }
      }
      prismicStandardBanner {
        data {
          announcement {
            text
          }
        }
      }
    }
  `)

  const [toggleTicker, setToggleTicker] = useState(false)

  useEffect(() => {
    if (!props.fixed || window.sessionStorage.getItem("fixedTickerHasShown"))
      return
    let ticker = document.querySelector(".fixed-ticker")
    setTimeout(() => {
      setToggleTicker(true)
      ticker.classList.remove("hide")
      ticker.classList.add("show")
    }, 2000)

    //Check if Fixed Ticker has already shown
    window.sessionStorage.setItem("fixedTickerHasShown", true)
  }, [])

  const handleClick = () => {
    if (!props.fixed) return
    let ticker = document.querySelector(".fixed-ticker")
    setToggleTicker(false)
    ticker.classList.add("hide")
    ticker.classList.remove("show")
    setTimeout(() => {
      ticker.style.display = "none"
    }, 1000)
  }

  return props.fixed ? (
    <FixedTicker onClick={handleClick} className={"fixed-ticker hide"}>
      <Ticker>
        {({ index }) => (
          <h1>{data.prismicAnnouncementBanner.data.announcement.text}</h1>
        )}
      </Ticker>
    </FixedTicker>
  ) : (
    <StandardTicker className={"standard-ticker"}>
      <Ticker>
        {({ index }) => (
          <h1>{data.prismicStandardBanner.data.announcement.text}</h1>
        )}
      </Ticker>
    </StandardTicker>
  )
}

export default SouvenirTicker
