import React from "react"
import PropTypes from "prop-types"

import styled from "styled-components"

const SizeSelect = styled.select`
  font-family: "Arial Narrow";
  border: 0;
  background: transparent;
  font-size: 1rem;
  margin-top: 1rem;
  outline: none;

  cursor: pointer;

  -moz-appearance: none;
  -webkit-appearance: none;
`

const StyledLabel = styled.label`
  font-family: "Arial Narrow";
  border: 0;
  background: transparent;
  font-size: 1rem;
  margin-top: 1rem;
  outline: none;

  cursor: pointer;

  -moz-appearance: none;
  -webkit-appearance: none;
`

const VariantSelector = props => {
  const { option } = props
  return (
    <>
      {/* <StyledLabel style={{ textTransform: "uppercase" }} htmlFor={option.name}>
        {option.name}{" "}
      </StyledLabel> */}
      <SizeSelect name={option.name} key={option.id} onChange={props.onChange}>
        <option value={"select"} key={"select"}>
          SELECT SIZE
          {/* <span style={{ textTransform: "uppercase" }}>{`${option.name}`}</span> */}
        </option>
        {option.values.map((value, index) => {
          return (
            <>
              <option value={value} key={`${option.name}-${value}`}>
                {`${value}`}
                {props.variants[index].availableForSale ? null : " (SOLD OUT)"}
              </option>
            </>
          )
        })}
      </SizeSelect>
      <br />
    </>
  )
}

VariantSelector.propTypes = {
  onChange: PropTypes.func,
  option: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    values: PropTypes.arrayOf(PropTypes.string),
  }),
}

export default VariantSelector
