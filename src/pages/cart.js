import React from "react"

import Cart from "../components/cart"

const CartPage = () => (
  <>
    <h1>Cart</h1>
    <Cart />
  </>
)

export default CartPage
