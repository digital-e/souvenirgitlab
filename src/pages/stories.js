import React from "react"

import StoriesGrid from "../components/storiesgrid"

import Footer from "../components/footer"

import SEO from "../components/seo"

const StoriesPage = props => {
  return (
    <>
      <SEO title="STORIES" />
      <StoriesGrid />
      <Footer />
    </>
  )
}

export default StoriesPage
