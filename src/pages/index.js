import React from "react"
import { Link, graphql } from "gatsby"

import SEO from "../components/seo"

import SlideShow from "../components/slideshow"

const IndexPage = props => (
  <>
    <SEO title="SOUVENIR OFFICIAL" />
    <SlideShow data={props.data.prismicSlideshow} />
  </>
)

export default IndexPage

export const query = graphql`
  query {
    prismicSlideshow {
      id
      data {
        gallery {
          image {
            url
            localFile {
              childImageSharp {
                fluid(maxWidth: 2500, quality: 90) {
                  ...GatsbyImageSharpFluid_noBase64
                }
              }
            }
          }
        }
        interval_time
      }
    }
  }
`
